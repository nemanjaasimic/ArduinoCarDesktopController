﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace ArduinoCarController.ViewModel
{
    public class BaseViewModel : INotifyPropertyChanged
    {
        /// <summary>
        /// Event which checks for changes
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// This method is called when some of the properties are changed
        /// </summary>
        /// <param name="propertyName">Name of the caller who made the change</param>
        protected virtual void NotifyPropertyChanged([CallerMemberName] String propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
