﻿using ArduinoCarController.Model;
using OxyPlot;
using OxyPlot.Series;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ArduinoCarController.ViewModel
{
    public sealed class LogWindowViewModel : BaseViewModel
    {
        private long logCount;

        private List<String> logs;

        private StringBuilder builder;

        public LogWindowViewModel()
        {
            builder = new StringBuilder();
            logCount = 0;
            logs = new List<string>()
            {
                "Logging started..."
            };

            PlotModel = new PlotModel { Title = "ArduinoCar Acceleration" };

            ForwardSeries.Color = OxyColors.Green;
            BackwardSeries.Color = OxyColors.Red;
            ZeroSeries.Color = OxyColors.Blue;

            ForwardSeries.MarkerFill = OxyColors.Green;
            BackwardSeries.MarkerFill = OxyColors.Red;
            ZeroSeries.MarkerFill = OxyColors.Blue;

            ForwardSeries.MarkerType = MarkerType.Circle;
            BackwardSeries.MarkerType = MarkerType.Circle;
            ZeroSeries.MarkerType = MarkerType.Circle;

            PlotModel.Series.Add(ForwardSeries);
            PlotModel.Series.Add(BackwardSeries);
            PlotModel.Series.Add(ZeroSeries);
        }

        public long LogCount
        {
            get => logCount;
            set
            {
                logCount = value;
                NotifyPropertyChanged();
            }
        }

        public string Logs
        {
            get => logs.Aggregate((i, j) => i + "\n" + j);
        }


        public void Log(Instruction instruction)
        {
            builder.Clear();
            builder.Append("[");
            builder.Append(DateTime.Now);
            builder.Append("] Code: ");
            builder.Append(instruction.Code);
            logs.Add(builder.ToString());
            LogCount++;
            NotifyPropertyChanged("Logs");
            
            if ((instruction.Equipment & (int)Equipment.TURN_ON) == (int)Equipment.TURN_ON)
            {
                if ((instruction.Commands & (int)Direction.FORWARD) == (int)Direction.FORWARD)
                {
                    ForwardSeries.Points.Add(new DataPoint(LogCount, instruction.AccelerationRate - 90));
                }
                else if ((instruction.Commands & (int)Direction.BACKWARD) == (int)Direction.BACKWARD)
                {
                    BackwardSeries.Points.Add(new DataPoint(LogCount, -(instruction.AccelerationRate - 90)));
                }
                else
                {
                    ZeroSeries.Points.Add(new DataPoint(LogCount, 0));
                }

                PlotModel.InvalidatePlot(true);
            }
        }


        public void ResetLog()
        {
            LogCount = 0;
            logs.Clear();
            logs.Add("Log cleared!");
            NotifyPropertyChanged("Logs");

            PlotModel.Series.ToList().ForEach((s => ((LineSeries)s).Points.Clear()));
            PlotModel.InvalidatePlot(true);
        }

        private LineSeries ForwardSeries { get; } = new LineSeries();

        private LineSeries BackwardSeries { get; } = new LineSeries();

        private LineSeries ZeroSeries { get; } = new LineSeries();

        public PlotModel PlotModel { get; }
    }
}
