﻿using ArduinoCarController.Config;
using ArduinoCarController.Model;
using ArduinoCarController.Util;
using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;

namespace ArduinoCarController.ViewModel
{
    public sealed class MainWindowViewModel : BaseViewModel
    {
        private NotifyCommandChanged notifyCommandChanged;

        private NotifyAccelChanged notifyAccelChanged;

        private NotifySteeringChanged notifySteeringChanged;

        private bool directionsEnabled;

        private bool hornBlinkEnabled;

        private Boolean connected;

        private HttpStatusCode connectionStatus;

        private String connectionStatusIcon;

        private InstructionRouter router;
        
        public MainWindowViewModel()
        {
            notifyCommandChanged += LastCommandSetter;
            notifyAccelChanged += AccelerationRateSetter;
            notifySteeringChanged += SteeringRateSetter;

            Generator = new InstructionGenerator(notifyCommandChanged, notifyAccelChanged, notifySteeringChanged);

            AccelerationRate = Generator.Instruction.AccelerationRate;
            SteeringRate = Generator.Instruction.SteeringRate;

            HornBlinkEnabled = true;
            DirectionsEnabled = false;
            connected = false;
            Speed = -10;
            connectionStatus = HttpStatusCode.ServiceUnavailable;
            ConnectionStatusIcon = ConfigUtil.ConnIcon("CONN_NO");
            router = InstructionRouter.Instance;
            
            ForwardCommand = new ActionCommand(Generator.Forward, () => DirectionsEnabled);
            ForwardUpCommand = new ActionCommand(Generator.ForwardUp, () => DirectionsEnabled);
            BackwardCommand = new ActionCommand(Generator.Backward, () => DirectionsEnabled);
            BackwardUpCommand = new ActionCommand(Generator.BackwardUp, () => DirectionsEnabled);
            LeftCommand = new ActionCommand(Generator.Left, () => DirectionsEnabled);
            LeftUpCommand = new ActionCommand(Generator.LeftUp, () => DirectionsEnabled);
            RightCommand = new ActionCommand(Generator.Right, () => DirectionsEnabled);
            RightUpCommand = new ActionCommand(Generator.RightUp, () => DirectionsEnabled);
            LeftTurnLightCommand = new ActionCommand(Generator.LeftTurnLight, AlarmConfig);
            RightTurnLightCommand = new ActionCommand(Generator.RightTurnLight, AlarmConfig);
            StartStopCommand = new ActionCommand(Generator.StartStop, AlarmConfig);
            HeadlightsCommand = new ActionCommand(Generator.Headlights, AlarmConfig);
            BlinkCommand = new ActionCommand(Generator.Blink, AlarmConfig);
            BlinkUpCommand = new ActionCommand(Generator.BlinkUp, AlarmConfig);
            AlarmCommand = new ActionCommand(Generator.Alarm, EngineConfig);
            HazardLightsCommand = new ActionCommand(Generator.HazardLights, AlarmConfig);
            HornCommand = new ActionCommand(Generator.Horn, AlarmConfig);
            HornUpCommand = new ActionCommand(Generator.HornUp, AlarmConfig);
            AccelerationRateUpCommand = new ActionCommand(Generator.AccelerationRateUp, () => true);
            AccelerationRateDownCommand = new ActionCommand(Generator.AccelerationRateDown, () => true);
            SteeringRateUpCommand = new ActionCommand(Generator.SteeringRateUp, () => true);
            SteeringRateDownCommand = new ActionCommand(Generator.SteeringRateDown, () => true);
            ConnectCommand = new ActionCommand(Connect, () => !connected);
            DisconnectCommand = new ActionCommand(Disconnect, () => connected);
            ResetCommand = new ActionCommand(Reset, () => connected);

            OpenLogCommand = new ActionCommand(OpenLog, () => true);
        }

        private InstructionGenerator Generator { get; }

        private void StartSendingInstruction() => 
            Task.Run(() => SendInstruction());

        private void StartShowingSpeed() =>
            Task.Run(() => ShowSpeed());

        private void SendInstruction()
        {
            while(connected)
            {
                Console.WriteLine($"Sending instruction: {Generator.Instruction.Code}");
                ConnectionStatus = router.Route(Generator.Instruction);
                Thread.Sleep(210);
            }
        }

        private void ShowSpeed()
        {
            while(connected)
            {
                if((Generator.Instruction.Equipment & (int)Equipment.TURN_ON) == (int)Equipment.TURN_ON)
                {
                    if((Generator.Instruction.Commands & (int)Direction.FORWARD) == (int)Direction.FORWARD
                        || (Generator.Instruction.Commands & (int)Direction.BACKWARD) == (int)Direction.BACKWARD)
                    {
                        Speed = Generator.Instruction.AccelerationRate - 90;
                    }
                    else
                    {
                        Speed = 0;
                    }

                }
                else
                {
                    Speed = -10;
                }
            }
        }

        public void Connect()
        {
            router.CreateConnection();
            connected = true;
            StartSendingInstruction();
            StartShowingSpeed();
        }

        public void Disconnect()
        {
            Generator.Instruction.Commands = (byte)Direction.NONE;
            Generator.Instruction.Equipment = (byte)Equipment.NONE;
            Generator.Instruction.AccelerationRate = 180;
            Generator.Instruction.SteeringRate = 160;
            router.Route(Generator.Instruction);
            connected = false;
            Speed = -10;
            // wait for RestClient to finish
            Thread.Sleep(1000);
            router.DisposeConnection();
            ConnectionStatus = HttpStatusCode.MethodNotAllowed;
        }

        public void Reset()
        {
            Disconnect();
            Connect();
        }

        private int _speed;

        public int Speed
        {
            get { return _speed; }
            set { _speed = value; NotifyPropertyChanged(); }
        }


        public int AccelerationRate
        {
            get => Generator.Instruction.AccelerationRate;
            set => AccelerationRateSetter(value, false);
        }

        private void AccelerationRateSetter(int value, bool generated)
        {
            if (generated)
                NotifyPropertyChanged("AccelerationRate");
            else
            {
                if (value >= 100 && value <= 255)
                {
                    Generator.Instruction.AccelerationRate = value;
                    NotifyPropertyChanged("AccelerationRate");
                }
            }
        }

        public int SteeringRate
        {
            get => Generator.Instruction.SteeringRate;
            set => SteeringRateSetter(value, false);
        }

        private void SteeringRateSetter(int value, bool generated)
        {
            if (generated)
                NotifyPropertyChanged("SteeringRate");
            else
            {
                if (value >= 100 && value <= 255)
                {
                    Generator.Instruction.SteeringRate = value;
                    NotifyPropertyChanged("SteeringRate");
                }
            }
        }

        public String ConnectionStatusIcon
        {
            get => connectionStatusIcon;
            set
            {
                connectionStatusIcon = ConfigUtil.ConnIcon(value);
                connectionStatusIcon = connectionStatusIcon ?? ConfigUtil.ConnIcon("CONN_NO");
                
                NotifyPropertyChanged();
            }
        }

        public HttpStatusCode ConnectionStatus
        {
            get => connectionStatus;
            set
            {
                if (value == HttpStatusCode.OK)
                    ConnectionStatusIcon = "CONN_OK";
                else if (value == HttpStatusCode.BadRequest)
                    ConnectionStatusIcon = "CONN_ERROR";
                else
                    ConnectionStatusIcon = "CONN_NO";
                connectionStatus = value;
            }
        }

        public String LastCommand
        {
            get => Generator.LastCommand;
            set => LastCommandSetter();
        }

        private void LastCommandSetter()
        {
            NotifyPropertyChanged("LastCommand");
        }

        public bool DirectionsEnabled
        {
            get => directionsEnabled;
            set
            {
                directionsEnabled = value;
                NotifyPropertyChanged();
            }
        }

        public bool HornBlinkEnabled
        {
            get => hornBlinkEnabled;
            set
            {
                hornBlinkEnabled = value;
                NotifyPropertyChanged();
            }
        }

        private bool AlarmConfig()
        {
            if((Generator.Instruction.Equipment & (int)Equipment.ALARM) == (int)Equipment.ALARM)
            {
                return HornBlinkEnabled = false;
            }
            return HornBlinkEnabled = true;
        }

        private bool EngineConfig()
        {
            if ((Generator.Instruction.Equipment & (int)Equipment.TURN_ON) == (int)Equipment.TURN_ON)
            {
                // if engine is on, enable driving controls
                DirectionsEnabled = true;
                return false;
            }
            DirectionsEnabled = false;
            return true;
        }
        
        private void OpenLog()
        {
            InstructionRouter.StartLogging();
        }
        
        public ICommand ForwardCommand { get; private set; }

        public ICommand ForwardUpCommand { get; private set; }

        public ICommand BackwardCommand { get;  private set; }

        public ICommand BackwardUpCommand { get; private set; }

        public ICommand LeftCommand { get; private set; }

        public ICommand LeftUpCommand { get; private set; }

        public ICommand RightCommand { get; private set; }

        public ICommand RightUpCommand { get; private set; }

        public ICommand LeftTurnLightCommand { get; private set; }

        public ICommand RightTurnLightCommand { get; private set; }

        public ICommand StartStopCommand { get; private set; }

        public ICommand HeadlightsCommand { get; private set; }

        public ICommand BlinkCommand { get; private set; }

        public ICommand BlinkUpCommand { get; private set; }

        public ICommand AlarmCommand { get; private set; }

        public ICommand HazardLightsCommand { get; private set; }

        public ICommand HornCommand { get; private set; }

        public ICommand HornUpCommand { get; private set; }
        
        public ICommand AccelerationRateUpCommand { get; private set; }

        public ICommand AccelerationRateDownCommand { get; private set; }

        public ICommand SteeringRateUpCommand { get; private set; }

        public ICommand SteeringRateDownCommand { get; private set; }

        public ICommand ConnectCommand { get; private set; }

        public ICommand DisconnectCommand { get; private set; }

        public ICommand ResetCommand { get; private set; }

        public ICommand OpenLogCommand { get; private set; }
    }
}