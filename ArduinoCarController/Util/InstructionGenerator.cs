﻿using ArduinoCarController.Model;
using System;

namespace ArduinoCarController.Util
{
    public delegate void NotifyCommandChanged();

    public delegate void NotifyAccelChanged(int value, bool generated);

    public delegate void NotifySteeringChanged(int value, bool generated);

    public class InstructionGenerator
    {
        private readonly int STEP = 10;

        private String lastCommand;
        
        private NotifyCommandChanged notifyCommandChanged;

        private NotifyAccelChanged notifyAccelChanged;

        private NotifySteeringChanged notifySteeringChanged;

        public Instruction Instruction { get; }

        public InstructionGenerator(NotifyCommandChanged commandAction, 
                                    NotifyAccelChanged accelAction, 
                                    NotifySteeringChanged steeringAction)
        {
            Instruction = new Instruction();
            notifyCommandChanged = commandAction;
            notifyAccelChanged = accelAction;
            notifySteeringChanged = steeringAction;
        }
        
        public String LastCommand
        {
            get => lastCommand;
            set
            {
                lastCommand = value;
                notifyCommandChanged();
            }
        }
        
        public void Forward()
        {
            if ((Instruction.Commands & (int)Direction.BACKWARD) != (int)Direction.NONE)
                Instruction.Commands = (Instruction.Commands | (int)Direction.FORWARD) & (int)Direction.NOT_BACKWARD;
            else
                Instruction.Commands = (int)(Instruction.Commands | (int)Direction.FORWARD);

            LastCommand = "FORWARD";
        }

        public void ForwardUp()
        {
            Instruction.Commands = Instruction.Commands & (int)Direction.NOT_FORWARD;
        }
        public void Backward()
        {
            if ((Instruction.Commands & (int)Direction.FORWARD) != (int)Direction.NONE)
                Instruction.Commands = (Instruction.Commands | (int)Direction.BACKWARD) & (int)Direction.NOT_FORWARD;
            else
                Instruction.Commands = Instruction.Commands | (int)Direction.BACKWARD;

            LastCommand = "BACKWARD";
        }

        public void BackwardUp()
        {
            Instruction.Commands = (byte)(Instruction.Commands & (byte)Direction.NOT_BACKWARD);
        }
        public void Left()
        {
            if ((Instruction.Commands & (int)Direction.RIGHT) != (int)Direction.NONE)
                Instruction.Commands = (Instruction.Commands | (int)Direction.LEFT) & (int)Direction.NOT_RIGHT;
            else
                Instruction.Commands = Instruction.Commands | (int)Direction.LEFT;

            LastCommand = "LEFT";
        }

        public void LeftUp()
        {
            Instruction.Commands = Instruction.Commands & (int)Direction.NOT_LEFT;
        }

        public void Right()
        {
            if ((Instruction.Commands & (int)Direction.LEFT) != (int)Direction.NONE)
                Instruction.Commands = (Instruction.Commands | (int)Direction.RIGHT) & (int)Direction.NOT_LEFT;
            else
                Instruction.Commands = Instruction.Commands | (int)Direction.RIGHT;

            LastCommand = "RIGHT";
        }

        public void RightUp()
        {
            Instruction.Commands = Instruction.Commands & (int)Direction.NOT_RIGHT;
        }

        public void LeftTurnLight()
        {
            if ((Instruction.Equipment & (int)Equipment.RIGHT_INDICATOR) == (int)Equipment.RIGHT_INDICATOR)
                Instruction.Equipment = Instruction.Equipment & (int)Equipment.RIGHT_INDICATOR_OFF;

            if ((Instruction.Equipment & (int)Equipment.HAZARD_LIGHTS) == (int)Equipment.HAZARD_LIGHTS)
                Instruction.Equipment = Instruction.Equipment & (int)Equipment.HAZARD_LIGHTS_OFF;

            // if its off, turn it on
            if ((Instruction.Equipment & (int)Equipment.LEFT_INDICATOR) == (int)Equipment.NONE)
                Instruction.Equipment = Instruction.Equipment | (int)Equipment.LEFT_INDICATOR;
            else
                // turn it off
                Instruction.Equipment = Instruction.Equipment & (int)Equipment.LEFT_INDICATOR_OFF;

            LastCommand = "TURN LEFT";
        }

        public void RightTurnLight()
        {
            if ((Instruction.Equipment & (int)Equipment.LEFT_INDICATOR) == (int)Equipment.LEFT_INDICATOR)
                Instruction.Equipment = Instruction.Equipment & (int)Equipment.LEFT_INDICATOR_OFF;

            if ((Instruction.Equipment & (int)Equipment.HAZARD_LIGHTS) == (int)Equipment.HAZARD_LIGHTS)
                Instruction.Equipment = Instruction.Equipment & (int)Equipment.HAZARD_LIGHTS_OFF;

            // if its off, turn it on
            if ((Instruction.Equipment & (int)Equipment.RIGHT_INDICATOR) == (int)Equipment.NONE)
                Instruction.Equipment = Instruction.Equipment | (int)Equipment.RIGHT_INDICATOR;
            else
                Instruction.Equipment = Instruction.Equipment & (int)Equipment.RIGHT_INDICATOR_OFF;

            LastCommand = "TURN RIGHT";
        }

        public void StartStop()
        {
            // if its off, turn it on
            if ((Instruction.Equipment & (int)Equipment.TURN_ON) == (int)Equipment.NONE)
            {
                Instruction.Equipment = Instruction.Equipment | (int)Equipment.TURN_ON;
                LastCommand = "TURN ON";
            }
            else
            {
                // turn it off
                Instruction.Equipment = Instruction.Equipment & (int)Equipment.TURN_OFF;
                LastCommand = "TURN OFF";
            }

        }

        public void Headlights()
        {
            // if its off, turn it on
            if ((Instruction.Equipment & (int)Equipment.HEADLIGHTS) == (int)Equipment.NONE)
                Instruction.Equipment = Instruction.Equipment | (int)Equipment.HEADLIGHTS;
            else
                // turn it off
                Instruction.Equipment = Instruction.Equipment & (int)Equipment.HEADLIGHTS_OFF;
            LastCommand = "LIGHTS";
        }

        public void Blink()
        {
            Instruction.Equipment = Instruction.Equipment | (int)Equipment.BLINK;

            LastCommand = "BLINK";
        }

        public void BlinkUp()
        {
            Instruction.Equipment = Instruction.Equipment & (int)Equipment.NOT_BLINK;
        }

        public void Alarm()
        {
            if ((Instruction.Equipment & (byte)Equipment.ALARM) == (byte)Equipment.NONE)
                Instruction.Equipment = Instruction.Equipment | (int)Equipment.ALARM;
            else
                // turn it off
                Instruction.Equipment = Instruction.Equipment & (int)Equipment.ALARM_OFF;

            LastCommand = "ALARM";
        }

        public void HazardLights()
        {
            // if its off, turn it on
            if ((Instruction.Equipment & (int)Equipment.HAZARD_LIGHTS) == (int)Equipment.NONE)
                Instruction.Equipment = Instruction.Equipment | (int)Equipment.HAZARD_LIGHTS;
            else
                // turn it off
                Instruction.Equipment = Instruction.Equipment & (int)Equipment.HAZARD_LIGHTS_OFF;

            LastCommand = "HAZARD L";
        }

        public void Horn()
        {
            Instruction.Equipment = Instruction.Equipment | (int)Equipment.HORN;

            LastCommand = "HORN";
        }

        public void HornUp()
        {
            Instruction.Equipment = Instruction.Equipment & (int)Equipment.HORN_OFF;
        }

        public void AccelerationRateUp()
        {
            if(Instruction.AccelerationRate + STEP > Instruction.MAX_RATE)
            {
                notifyAccelChanged(Instruction.AccelerationRate = Instruction.MAX_RATE, true);
            }
            else
            {
                notifyAccelChanged(Instruction.AccelerationRate += STEP, true);
            }
            LastCommand = "ACCEL UP";
        }

        public void AccelerationRateDown()
        {
            if (Instruction.AccelerationRate - STEP < Instruction.MIN_RATE)
            {
                notifyAccelChanged(Instruction.AccelerationRate = Instruction.MIN_RATE, true);
            }
            else
            {
                notifyAccelChanged(Instruction.AccelerationRate -= STEP, true);
            }
            LastCommand = "ACCEL DOWN";
        }

        public void SteeringRateUp()
        {
            if (Instruction.SteeringRate + STEP > Instruction.MAX_RATE)
            {
                notifySteeringChanged(Instruction.SteeringRate = Instruction.MAX_RATE, true);
            }
            else
            {
                notifySteeringChanged(Instruction.SteeringRate += STEP, true);
            }
            LastCommand = "STEER UP";
        }
        public void SteeringRateDown()
        {
            if (Instruction.SteeringRate - STEP < Instruction.MIN_RATE)
            {
                notifySteeringChanged(Instruction.SteeringRate = Instruction.MIN_RATE, true);
            }
            else
            {
                notifySteeringChanged(Instruction.SteeringRate -= STEP, true);
            }
            LastCommand = "STEER DOWN";
        }
    }
}
