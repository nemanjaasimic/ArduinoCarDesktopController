﻿using System;
using System.Configuration;

namespace ArduinoCarController.Config
{
    public static class ConfigUtil
    {

        public static String IPAddress => 
            ConfigurationManager.AppSettings.Get("IP");

        public static String UriString =>
            ConfigurationManager.AppSettings.Get("URI");

        public static String ConnIcon(String type) =>
            ConfigurationManager.AppSettings.Get(type);
    }
}
