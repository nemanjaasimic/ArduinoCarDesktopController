﻿using ArduinoCarController.Model;
using RestSharp;
using System;
using System.Net;

namespace ArduinoCarController.Util
{
    public class WiFiConnector
    {
        public IPAddress IPAddress { get; }

        private RestClient Client { get; }
        
        public WiFiConnector(String uri, String ip)
        {
            IPAddress = IPAddress.Parse(ip);
            try
            {
                Client = new RestClient()
                {
                    BaseUrl = new Uri(uri),
                    // five seconds timeout
                    Timeout = 500
                };
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12
                                                        | SecurityProtocolType.Tls11
                                                        | SecurityProtocolType.Tls;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public HttpStatusCode Send(Instruction instruction)
        {
            if(Client != null)
            {
                try
                {
                    RestRequest request = new RestRequest(Method.POST);
                    request.AddHeader("content-type", "application/x-www-form-urlencoded");
                    request.AddParameter("application/x-www-form-urlencoded", 
                                        $"direction={instruction.Commands.ToString()}&speed={instruction.AccelerationRate.ToString()}" +
                                        $"&steering={instruction.SteeringRate.ToString()}&equipment={instruction.Equipment.ToString()}", 
                                        ParameterType.RequestBody);
                    IRestResponse response = Client.Execute(request);

                    return response.StatusCode;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            }
            return HttpStatusCode.MethodNotAllowed;
        }   
    }
}