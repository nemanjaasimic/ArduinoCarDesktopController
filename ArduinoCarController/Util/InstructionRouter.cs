﻿using ArduinoCarController.Config;
using ArduinoCarController.Model;
using ArduinoCarController.View;
using System;
using System.Net;
using System.Threading.Tasks;

namespace ArduinoCarController.Util
{
    public sealed class InstructionRouter
    {
        private static InstructionRouter instance;

        private static readonly Object padlock = new object();

        private static WiFiConnector connection;

        private static LogWindow logger;

        public static bool logActive;

        public static InstructionRouter Instance
        {
            get
            {
                lock (padlock)
                {
                    if (instance == null)
                        instance = new InstructionRouter();
                    return instance;
                }
            }
        }

        InstructionRouter() =>
            CreateConnection();

        public HttpStatusCode Route(Instruction instruction)
        {
            if(connection != null)
            {
                if (logActive)
                    Task.Run(() => logger.Log(instruction));
                return connection.Send(instruction);
            }
            return HttpStatusCode.MethodNotAllowed;
        }

        public void CreateConnection() =>
            connection = new WiFiConnector(ConfigUtil.UriString, ConfigUtil.IPAddress);

        public void DisposeConnection() => 
            connection = null;

        public static void StartLogging()
        {
            if (!logActive)
            {
                logger = new LogWindow();
                logger.Show();
                logActive = true;
            }
        }
    }
}
