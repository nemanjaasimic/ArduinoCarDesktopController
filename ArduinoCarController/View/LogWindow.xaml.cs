﻿using ArduinoCarController.ViewModel;
using System.ComponentModel;
using System.Windows;
using ArduinoCarController.Model;
using ArduinoCarController.Util;
using System.Windows.Controls;

namespace ArduinoCarController.View
{
    /// <summary>
    /// Interaction logic for LogWindow.xaml
    /// </summary>
    public partial class LogWindow : Window
    {
        private readonly LogWindowViewModel viewModel;

        public LogWindow()
        {
            viewModel = new LogWindowViewModel();
            DataContext = viewModel;
            InitializeComponent();
        }
        
        public void Close_Click(object sender, CancelEventArgs e)
        {
            InstructionRouter.logActive = false;
        }

        public void Log(Instruction instruction)
        {
            viewModel.Log(instruction);
        }

        public void TextChanged_Event(object sender, TextChangedEventArgs e)
        {
            logTextBox.ScrollToEnd();
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            logTextBox.Clear();
            viewModel.ResetLog();
        }
    }
}
