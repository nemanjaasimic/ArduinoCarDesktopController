﻿using System.Windows;
using System.Windows.Input;

namespace ArduinoCarController.LocalBinding
{
    public class KeyUpBinding : InputBinding
    {
        public Key Key
        {
            get => (Key)GetValue(KeyProperty); 
            set => SetValue(KeyProperty, value); 
        }

        public static readonly DependencyProperty KeyProperty =
            DependencyProperty.Register("Key", typeof(Key), typeof(KeyUpBinding), new PropertyMetadata(Key.None, KeyChanged));

        private static void KeyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var keyBinding = (d as KeyUpBinding);

            Keyboard.AddKeyUpHandler(App.Current.MainWindow, (s, ku) =>
            {
                if (keyBinding.Command != null && ku.Key == keyBinding.Key && ku.IsUp)
                {
                    keyBinding.Command.Execute(null);
                }
            });
        }

        new public ICommand Command
        {
            get => (ICommand)GetValue(CommandProperty);
            set => SetValue(CommandProperty, value);
        }

        new public static readonly DependencyProperty CommandProperty =
            DependencyProperty.Register("Command", typeof(ICommand), typeof(KeyUpBinding), new PropertyMetadata(null));
    }
}
