﻿using System;

namespace ArduinoCarController.Model
{
    /// <summary>
    /// This class is responsible for keeping the instruction 
    /// that controls the car. It has data about: 
    /// direction(<see cref="Commands"/>)
    /// speed(<see cref="AccelerationRate"/>)
    /// steering sensitivity(<see cref="SteeringRate"/>)
    /// additional car electronics(<see cref="Equipment"/>)
    /// It also provides unified instruction word as a String. <see cref="Code"/>
    /// </summary>
    public class Instruction
    {   
        public static readonly int MAX_RATE = 255;

        public static readonly int MIN_RATE = 100;

        private int accelerationRate;

        private int steeringRate;

        /// <summary>
        /// Constructs instruction with default <see cref="AccelerationRate"/>
        /// and <see cref="SteeringRate"/>
        /// </summary>
        public Instruction()
        {
            accelerationRate = 180;
            steeringRate = 160;
        }

        /// <summary>
        /// Code for instruction. 
        /// Use <see cref="Commands"/> + <see cref="AccelerationRate"/>
        /// + <see cref="SteeringRate"/> + <see cref="Equipment"/> 
        /// to generate the instruction using bitwise operators.
        /// Format of the instruction code is:
        /// |LEFT|RIGHT|BACKWARD|FORWARD|DRIVE RATE|STEERING RATE|HORN|HAZARD LIGHT|ALARM|BLINK|HEADLIGHTS|ON/OFF|RIGHT TURNLIGHT|LEFT TURNLIGHT|
        /// </summary>
        public string Code
        {
            get
            {
                return Convert.ToString(Commands, 2).PadLeft(4, '0') + Convert.ToString(AccelerationRate)
                    + Convert.ToString(SteeringRate) + Convert.ToString(Equipment, 2).PadLeft(8, '0');
            }
        }

        /// <summary>
        /// Has the 4-bit instruction for driving and steering.
        /// Use <see cref="Direction"/> to generate this code.
        /// |LEFT|RIGHT|BACKWARD|FORWARD|
        /// </summary>
        public int Commands { get; set; } = (int)Direction.NONE;

        /// <summary>
        /// Rate of the direction in which car moves.
        /// FORWARD &lt;-&gt; BACKWARD
        /// Interval 100-255
        /// </summary>
        public int AccelerationRate
        {
            get
            {
                return accelerationRate;
            }

            set
            {
                if (value >= MIN_RATE && value <= MAX_RATE)
                    accelerationRate = value;
                else
                    throw new ArgumentOutOfRangeException($"Can't set rate to {value}");
            }
        }

        /// <summary>
        /// Rate of the direction in which car steers.
        /// LEFT &lt;-&gt; RIGHT
        /// Interval 100-255
        /// </summary>
        public int SteeringRate
        {
            get
            {
                return steeringRate;
            }
            set
            {
                if (value >= MIN_RATE && value <= MAX_RATE)
                    steeringRate = value;
                else
                    throw new ArgumentOutOfRangeException($"Can't set rate to {value}");
            }
        }
        /// <summary>
        /// All of the equipment which car has.
        /// Use <see cref="Model.Equipment"/> to generate this code.
        /// |HORN|HAZARD LIGHT|ALARM|BLINK|HEADLIGHTS|ON/OFF|RIGHT TURNLIGHT|LEFT TURNLIGHT|
        /// </summary>
        public int Equipment { get; set; } = (int)Model.Equipment.NONE;
        
    }
}
