﻿namespace ArduinoCarController.Model
{
    public enum Direction
    {
        NONE        = 0b0000,
        FORWARD     = 0b0001,
        BACKWARD    = 0b0010,
        RIGHT       = 0b0100,
        LEFT        = 0b1000,

        // Used for masking directions
        NOT_FORWARD     = 0b1110,
        NOT_BACKWARD    = 0b1101,
        NOT_RIGHT       = 0b1011,
        NOT_LEFT        = 0b0111
    }
}
