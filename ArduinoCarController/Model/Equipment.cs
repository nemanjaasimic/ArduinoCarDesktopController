﻿namespace ArduinoCarController.Model
{
    public enum Equipment
    {
        NONE                = 0b0000_0000,
        LEFT_INDICATOR      = 0b0000_0001,
        RIGHT_INDICATOR     = 0b0000_0010,
        TURN_ON             = 0b0000_0100,
        HEADLIGHTS          = 0b0000_1000,
        BLINK               = 0b0001_0000,
        ALARM               = 0b0010_0000,
        HAZARD_LIGHTS       = 0b0100_0000,
        HORN                = 0b1000_0000,

        // Used for masking equipment
        ALL_ON              = 0b1111_1111,
        LEFT_INDICATOR_OFF  = 0b1111_1110,
        RIGHT_INDICATOR_OFF = 0b1111_1101,
        TURN_OFF            = 0b1111_1011,
        HEADLIGHTS_OFF      = 0b1111_0111,
        NOT_BLINK           = 0b1110_1111,
        ALARM_OFF           = 0b1101_1111,
        HAZARD_LIGHTS_OFF   = 0b1011_1111,
        HORN_OFF            = 0b0111_1111
    }
}
